//=======STOCKAGE DES MOTS==========

const firstPart = ["Malgré ", "Compte tenu ", "Quelle que soit ", "En ce qui concerne ", "Considérant "];
const secondPart = ["cette rigueur ", "la morosité ", "la situation ", "l'ambiance ", "la fragilité "];
const thirdPart = ["présente,", "actuelle,", "induite,", "observée,", "contextuelle,"];
const fourthPart = [" il convient ", " on se doit ", " il est préférable ", " on ne peut se passer ", " il serait bon "];
const fifthPart = ["d'aller de l'avant", "d'anticiper la prochaine action", "d'uniformiser ces décisions", "d'imaginer les améliorations possibles", "d'analyser les alternatives"];

//=====FONCTION=========

function random(array) {

    return array[Math.floor(Math.random() * array.length)]
}

console.log("Bienvenue dans le générateur de citation !\n 1: Générer une citation\n 0: Quitter le programme");

var option = 2;

while (option) {
    var option = Number(prompt("Choisisez une option !"));
    switch (option) {
        case 1:
            let first = random(firstPart);
            let second = random(secondPart);
            let third = random(thirdPart);
            let fourth = random(fourthPart);
            let fifth = random(fifthPart);

            //ASSEMBLAGE DES MOTS
            var compo = (" \" " + first + second + third + fourth + fifth + " . \" \n ");

            console.log(compo);
            break;
        case 0:
            console.log("Au revoir!");
            break;
        default:
            console.log("Option non valide");
            break;
    }
}
