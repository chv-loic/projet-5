function citation() {

    //AFFICHAGE DE LA SECTION PHRASE
    document.getElementById('phrase').style.display = "block";

    //EFFACER LES ANCIENNES CITATIONS A CHAQUES CLIQUES
    document.getElementById('phrase').innerHTML = "";

    //RECUPERATION DU NOMBRE DE CITATIONS DEMANDE
    let nbCitation = document.getElementById("nbcitation").value;
}

//=======STOCKAGE DES MOTS==========

const firstPart = ["Malgré ", "Compte tenu ", "Quelle que soit ", "En ce qui concerne ", "Considérant "];
const secondPart = ["cette rigueur ", "la morosité ", "la situation ", "l'ambiance ", "la fragilité "];
const thirdPart = ["présente,", "actuelle,", "induite,", "observée,", "contextuelle,"];
const fourthPart = [" il convient ", " on se doit ", " il est préférable ", " on ne peut se passer ", " il serait bon "];
const fifthPart = ["d'aller de l'avant", "d'anticiper la prochaine action", "d'uniformiser ces décisions", "d'imaginer les améliorations possibles", "d'analyser les alternatives"];

//=====FONCTION=========

function random(array) {

    return array[Math.floor(Math.random() * array.length)]
}

console.log("Bienvenue dans le générateur de citation !");

var option = 2;

while (option) {

    var option = Number(prompt("Choisisez une option !\n 1: Générer une citation\n 0: Quitter le programme"));

    switch (option) {
        case 1:

            var nbCitation = prompt(("Combien de citation souhaitez-vous générez ? (Max 5)"));
            
            console.log("Vous avez choisi de générez " + nbCitation + " citations");

            var typeCitation = prompt(("Quel type de citations souhaitez-vous générez ? \n 1:Classic \n 2:Yoda"));
            
            console.log("Vous avez sélectionnez le type " + typeCitation);

            for (i = 1; i <= nbCitation; i++) {

                let first = random(firstPart);
                let second = random(secondPart);
                let third = random(thirdPart);
                let fourth = random(fourthPart);
                let fifth = random(fifthPart);

                if (typeCitation == 1) {
                    //ASSEMBLAGE DES MOTS
                    var compo = (" \" " + first + second + third + fourth + fifth + " . \" \n ");

                    console.log(compo);
                } else if (typeCitation == 2) {
                    //ASSEMBLAGE DES MOTS
                    var compo = (" \" " + first + second + third + fifth + fourth + " . \" \n ");

                    console.log(compo);
                } else {
                    console.log("Saisie incorrecte");
                }
            }
            break;
        case 0:
            console.log("Au revoir!");
            break;
        default:
            console.log("Option non valide");
            break;
    }
}
