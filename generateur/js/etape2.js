//=======STOCKAGE DES MOTS==========

const firstPart = ["Malgré ", "Compte tenu ", "Quelle que soit ", "En ce qui concerne ", "Considérant "];
const secondPart = ["cette rigueur ", "la morosité ", "la situation ", "l'ambiance ", "la fragilité "];
const thirdPart = ["présente,", "actuelle,", "induite,", "observée,", "contextuelle,"];
const fourthPart = [" il convient ", " on se doit ", " il est préférable ", " on ne peut se passer ", " il serait bon "];
const fifthPart = ["d'aller de l'avant", "d'anticiper la prochaine action", "d'uniformiser ces décisions", "d'imaginer les améliorations possibles", "d'analyser les alternatives"];

//=====FONCTION=========

function random(array){
    
    return array[Math.floor(Math.random() * array.length)]
}

function citation() {

    //AFFICHAGE DE LA SECTION PHRASE
    document.getElementById('phrase').style.display="block";
    
    //EFFACER LES ANCIENNES CITATIONS A CHAQUES CLIQUES
    document.getElementById('phrase').innerHTML = "";

    //RECUPERATION DU NOMBRE DE CITATIONS DEMANDE
    let nbCitation = document.getElementById("nbcitation").value;

    for (i = 1; i <= nbCitation; i++) {
    
            let first = random(firstPart);
            let second = random(secondPart);
            let third = random(thirdPart);
            let fourth = random(fourthPart);
            let fifth = random(fifthPart);
        
        if (document.getElementById('type1').checked) {
            
            //ASSEMBLAGE DES MOTS
            var compo = (" \" " + first + second + third + fourth + fifth + " . \" \n ");
            
        }else if(document.getElementById('type2').checked){
            
            //ASSEMBLAGE DES MOTS
            var compo = (" \" " + first + second + third + fifth + fourth + " . \" \n ");
        }
        
            //AFFICHAGE DES CITATIONS
            let p = document.createElement("P")
            let afficher = document.createTextNode(compo);
            p.appendChild(afficher);
            document.getElementById("phrase").appendChild(p);
    }
}